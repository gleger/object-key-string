'user strict';


/**
 * Return value of chaine access describe in keyString argument
 * @example
 * const oks = require('object-key-string');
 *
 * let object = {
 *   name: 'Durand',
     adress: [
       {
         street: 'Champs-Élysée',
         number: 28,
         city: 'Grenoble'
       }
     ]
 * };
 * console.log(oks(object, 'name')); // return `Durand`
 * console.log(oks(object, 'adress.0.street')); // Return `Champs-Élysée`
 */
module.exports = function oks(object, keyString) {
    if (typeof keyString !== 'string') {
        throw new Error('keyString must be a string');
    }

    if (!keyString.length) {
        return object;
    }

    let splited = keyString.split('.');
    return oks(object[splited[0]], splited.slice(1).join('.'));
}