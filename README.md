# object-key-string

Return value of member in complex object with single key-string like `some.attr.to.get.0.value`.

Numbers in key-string are index in array.

```{js}
const oks = require('object-key-string');

let object = {
    name: 'Durand',
    adress: [
        {
            street: 'Champs-Élysée',
            number: 28,
            city: 'Grenoble'
        }
    ]
};

console.log(oks(object, 'name'));
// return `Durand`

console.log(oks(object, 'adress.0.street'));
// Return `Champs-Élysée`
```
