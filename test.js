const oks = require('./');
let object = {
    name: 'Durand',
    adress: [{
        street: 'Champs-Élysée',
        number: 28,
        city: 'Grenoble'
    }]
};
console.log(oks(object, 'name')); // return `Durand`
console.log(oks(object, 'adress.0.street')); // Return `Champs-Élysée